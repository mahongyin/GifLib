GifLibDecoder 解析io InputStream 实际是获取byte[]交给下面的解析器
GifLibByteBufferDecoder  解析 byte[]生成 GifDrawable的 包装 GifLibDrawableResource
GifLibDrawableResource   封装GifDrawable提供销毁和内存占用大小计算（用于lrucache）
DrawableBytesTranscoder和GifLibBytesTranscoder  用于转换
GifLibEncoder            用于序列化成文件


根据上机实际表现android-gif-drawable，内存占用和Cpu占用率最好，而且提供了pl.droidsonroids.gif.GifDrawable并且拥有解析和序列化的api，而且作者在持续维护，后期bug修复和项目其他需求支持均可以兼顾，选择此第三方库为gif解析和渲染核心

Registry api说明
append(..) 追加到最后，当内部的组件在 handles()返回false或失败时候使用追加组件
prepend(..)最佳到前面，当你的组件在失败时候使用原生提供组件
replace(..)替换组件