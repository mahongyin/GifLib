package com.mhy.glide.giflib;

import android.support.rastermill.FrameSequenceDrawable;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.annotation.GlideType;
import com.bumptech.glide.annotation.GlideExtension;
import com.bumptech.glide.request.RequestOptions;

/**
 * @author CTVIT
 */
@GlideExtension
public class MyGifExtension {

    private MyGifExtension() {
    }

    final static RequestOptions DECODE_TYPE = RequestOptions.decodeTypeOf(FrameSequenceDrawable.class).lock();
/**
 * 命名新的asGifLib方法
 */
    @GlideType(FrameSequenceDrawable.class)
    public static RequestBuilder<FrameSequenceDrawable>asGifLib(RequestBuilder<FrameSequenceDrawable> requestBuilder) {
       return requestBuilder.apply(DECODE_TYPE);
    }
}
