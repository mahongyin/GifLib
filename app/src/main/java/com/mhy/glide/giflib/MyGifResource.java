package com.mhy.glide.giflib;

import android.support.rastermill.FrameSequenceDrawable;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.resource.drawable.DrawableResource;

/**
 * 创建一个用于加载 GIF 的 Glide 的 Resource
 */
public class MyGifResource extends DrawableResource<FrameSequenceDrawable> {

    public MyGifResource(FrameSequenceDrawable drawable) {
        super(drawable);
    }

    @NonNull
    @Override
    public Class<FrameSequenceDrawable> getResourceClass() {
        return FrameSequenceDrawable.class;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public void recycle() {
        drawable.stop();
        drawable.destroy();
    }
}
