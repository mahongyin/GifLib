package com.mhy.glide.progress;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created By Mahongyin
 * Date    2020/8/24 11:30
 * @author mahongyin
 */
public class HeadInterceptor implements Interceptor {
    Map<String, String> map;

    private HeadInterceptor() {}

    public HeadInterceptor(Map<String, String> headers) {
        map = headers;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder request = chain.request().newBuilder();
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                //这里添加我们需要的请求头
                request.addHeader(entry.getKey(), entry.getValue());
            }
        }
        return chain.proceed(request.build());
    }
}