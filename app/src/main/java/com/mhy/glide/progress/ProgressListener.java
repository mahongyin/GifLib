package com.mhy.glide.progress;

/**
 * Created By Mahongyin
 * Date    2020/8/24 10:15
 */
public interface ProgressListener {
    /**
     * 加载进度
     * @param progress
     */
    void onProgress(int progress);
}
