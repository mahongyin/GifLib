package com.mhy.glide.sample;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.mhy.glide.GlideUtil;
import com.mhy.glide.R;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "mygif";

    private ImageView imageView;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.iv);
        tv = findViewById(R.id.tv);
        int pid = android.os.Process.myPid();
        int maxMemory = (int) Runtime.getRuntime().maxMemory() / 1024 / 1024;
        Log.i(TAG, "机型：" + android.os.Build.MODEL + " 可获取应用程序最大可用内存为：" + maxMemory + "M");
        Log.d(TAG, "oncreate====" + "pid=" + pid);
        Log.d(TAG, "memory" + getPenter());
        final String gif = "https://pica.zhimg.com/50/v2-12461f1d25b1260da44b40a0ef685547_720w.gif?source=1940ef5c";
//        final String gif = "https://github.com/bm-x/PhotoView/raw/master/demo1.gif";
        final String png = "https://liuyouth.github.io/img/root_icon.png";
//        GlideApp.with(this).as(FrameSequenceDrawable.class).load(gif).into(imageView);
//        GlideApp.with(this).asGifLib().load(gif).into(imageView);
//        GlideApp.with(this).load(png).into(imageView);
//        Glide.with(this).asGif().load(gif).into(imageView);
        GlideUtil.showGif(this, gif, imageView);
//        GlideUtil.showProgressImage(this, gif, imageView, new LoadListener() {
//            @Override
//            public void onProgress(final int progress) {
//                Log.e("onProgress: " ,""+ progress);
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        tv.setText(progress+"%");
//                    }
//                });
//            }
//        });
        InputStream inputStream = getResources().openRawResource(R.raw.timg);
        GifImageView gifImageView = (GifImageView) findViewById(R.id.gif_loading);
//        GlideUtil.showGif2(this,inputStream,gifImageView);
        GlideUtil.showGif2(this,gif,gifImageView);
    }

    public String getPenter() {
        String dir = "/proc/meminfo";
        try {
            FileReader fr = new FileReader(dir);
            BufferedReader br = new BufferedReader(fr, 2048);
            String memoryLine = br.readLine();
            String subMemoryLine = memoryLine.substring(memoryLine.indexOf("MemTotal:"));
            br.close();
            long totalMemorySize = Integer.parseInt(subMemoryLine.replaceAll("\\D+", ""));
            long availableSize = getAvailableMemory(MainActivity.this) / 1024;
            int percent = (int) ((totalMemorySize - availableSize) / (float) totalMemorySize * 100);
            return percent + "%";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "无结果";
    }

    private long getAvailableMemory(Context context) {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        getActivityManager(context).getMemoryInfo(mi);
        return mi.availMem;
    }

    ActivityManager mActivityManager;

    public ActivityManager getActivityManager(Context context) {
        if (mActivityManager == null) {
            mActivityManager = (ActivityManager)
                    context.getSystemService(Context.ACTIVITY_SERVICE);
        }
        return mActivityManager;
    }

}
